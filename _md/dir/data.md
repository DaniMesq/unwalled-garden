## Data directory `unwalled.garden/dir/data`

---

 - Folder type
 - **Description**: Contains the unwalled.garden record files.
 - **Path**: `/.data/unwalled.garden`

---

### File structure

|Path|Type|
|-|-|
|`/bookmarks/*.json`|[Bookmark](/bookmark)|
|`/comments/*.json`|[Comment](/comment)|
|`/discussions/*.json`|[Discussion](/discussion)|
|`/follows.json`|[Follows](/follows)|
|`/media/*.json`|[Media](/media)|
|`/posts/*.json`|[Post](/post)|
|`/reactions/*.json`|[Reaction](/reaction)|
|`/sitelists/*.json`|[Sitelist](/sitelist)|
|`/votes/*.json`|[Vote](/vote)|
